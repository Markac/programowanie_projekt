import turtle
zolw=turtle.Turtle()
import math
turtle.setworldcoordinates(-400,0,400,800)

zolw.shape("turtle")

dom_szer=600
parter_wys=200
drzwi_szer=50
drzwi_wys=90
dach=(math.sqrt(((dom_szer)**2)/2))

predkosc=1

zolw.backward((dom_szer)/2)
zolw.left(90)
zolw.forward(parter_wys)
zolw.right(90)
zolw.forward(dom_szer)
zolw.right(90)
zolw.forward(parter_wys)
zolw.right(90)
zolw.forward((dom_szer)/2)
zolw.right(90)
zolw.forward(drzwi_wys)
zolw.right(90)
zolw.forward(drzwi_szer)
zolw.right(90)
zolw.forward(drzwi_wys)
zolw.left(90)
zolw.forward(((dom_szer)/2) - (drzwi_szer))
zolw.left(90)
zolw.forward(parter_wys)
zolw.left(45)
zolw.forward(dach)
zolw.left(90)
zolw.forward(dach)

okno=turtle.getscreen()
turtle.mainloop()